Lamorak Vim
===========

This is my personal Vim configuration. It works exactly like vanilla Vim, with
a few (and only a ever few) useful new features. It also has an expanded tutor,
documenting features as well as offering a few tips.

I still update and test this package every so often, but I am now using
[ex-vi](http://ex-vi.sourceforge.net/), since I decided that
a lighter text editor is sufficient.

Installation
------------
Make sure you have CTags, Vim and Source Code Pro installed.
	$ cp vim -R ~/.vim
	$ cp vimrc ~/.vimrc
	$ gvim tutor