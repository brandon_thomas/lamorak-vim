﻿"==============================================================================
"       "          _                                    _              "
"       "         | |    __ _ _ __ ___   ___  _ __ __ _| | __          "
"       "         | |   / _` | '_ ` _ \ / _ \| '__/ _` | |/ /          "
"       "         | |__| (_| | | | | | | (_) | | | (_| |   <           "
"       "         |_____\__,_|_| |_| |_|\___/|_|  \__,_|_|\_\          "
"                   "A state of the art Vim distribution."
" Version: 1.1.0
"==============================================================================

" This file contains the configuration for Vim. To open a fold, place
" the cursor over it and type zo. To open all folds type z0.

" Initialize settings. {

set nocompatible
runtime bundle/vim-pathogen-master/autoload/pathogen.vim
execute pathogen#infect()

" }

" <leader> key bindings. {

" Set <leader> as space.
let mapleader = ' '

" Use <leader>c to clear screen (and searches, and fuzzy search cache).
nnoremap <leader>c :nohl<CR><F5><C-L>

" Use <leader>s to jump to a word quickly.
map <leader>s <Plug>(easymotion-s2)

" Use <leader>w to jump to a word forward quickly.
map <leader>w <Plug>(easymotion-w)

" Use <leader>b to jump backwards to a word.
map <leader>b <Plug>(easymotion-b)

" Use <leader>f to jump to a character.
map <leader>f <Plug>(easymotion-f)

" Use <leader>g to toggle indent guides.
map <leader>g :IndentGuidesToggle<cr>

" Use <leader>p to fuzzy search files.
map <leader>p <C-p>

" Use <leader>s1-9 to save session.
map <leader>s1 :SaveSession! 1<cr>
map <leader>s2 :SaveSession! 2<cr>
map <leader>s3 :SaveSession! 3<cr>
map <leader>s4 :SaveSession! 4<cr>
map <leader>s5 :SaveSession! 5<cr>
map <leader>s6 :SaveSession! 6<cr>
map <leader>s7 :SaveSession! 7<cr>
map <leader>s8 :SaveSession! 8<cr>
map <leader>s9 :SaveSession! 9<cr>

" Use <leader>l1-9 to load session.
map <leader>l1 :OpenSession 1<cr>
map <leader>l2 :OpenSession 2<cr>
map <leader>l3 :OpenSession 3<cr>
map <leader>l4 :OpenSession 4<cr>
map <leader>l5 :OpenSession 5<cr>
map <leader>l6 :OpenSession 6<cr>
map <leader>l7 :OpenSession 7<cr>
map <leader>l8 :OpenSession 8<cr>
map <leader>l9 :OpenSession 9<cr>

" Use <leader>r to remove trailing whitespace
nnoremap <leader>r :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

" Use z# to change fold level.
set foldmethod=indent
set nofoldenable
nmap z0 :set nofoldenable<CR>
nmap z1 :set foldenable foldlevel=0<CR>
nmap z2 :set foldenable foldlevel=1<CR>
nmap z3 :set foldenable foldlevel=2<CR>
nmap z4 :set foldenable foldlevel=3<CR>
nmap z5 :set foldenable foldlevel=4<CR>
nmap z6 :set foldenable foldlevel=5<CR>
nmap z7 :set foldenable foldlevel=6<CR>
nmap z8 :set foldenable foldlevel=7<CR>
nmap z9 :set foldenable foldlevel=8<CR>

" Type <leader>t to toggle tagbar.
nmap <leader>t :TagbarOpenAutoClose<cr>

" Type <leader>u to toggle undo tree.
nmap <leader>u :UB<cr>

" Use <leader>-y to go through yank stack.
let g:yankstack_map_keys = 0
nmap <leader>y <Plug>yankstack_substitute_older_paste

" }

" Ctrl key bindings. {

" Use <C-n> to create new cursor.
let g:multi_cursor_next_key='<C-n>'

" Use <C-x> to skip cursor.
let g:multi_cursor_skip_key='<C-x>'

" Use <C-p> unselect last cursor.
let g:multi_cursor_prev_key='<C-p>'

" Use <C-e>h and <C-e>l to move between buffers.
map <C-e>h :bp<cr>
map <C-e>l :bn<cr>

" Use <C-e>c to close a buffer.
map <C-e>c :bd!<cr>

" Use <C-t>h and <C-t>l to move between tabs.
map <C-t>h :tabp<cr>
map <C-t>l :tabl<cr>

" Use <C-t>n to create a new tab.
map <C-t>n :tabe<cr>

" Use <C-t>c to close a tab.
map <C-t>c :tabc<cr>

" }

" Additional commands. {

" Type ; instead of : for commands.
nnoremap ; :

" Type :Hex to use hex mode.
command Hex %!xxd

" Type :Nohex to use regular mode.
command Nohex %!xxd -r

" Type :Ctags to set up tags.
command Ctags silent !ctags -R "%:p:h" -F "%:p:h/tags"

" Save as sudo with :w!!
cmap w!! w !sudo tee > /dev/null %

" }

" Defines the behaviour of text. {

" File encoding to use on default (new file).
set fileencoding=utf-8
set fileformat=unix
"set bomb

" Use uft-8 internally.
set encoding=utf-8

" Set tab to 8 spaces.
set tabstop=8
set shiftwidth=8
set softtabstop=8

" Wrap lines when 'gq' is pressed to 80 characters.
autocmd filetype * set formatoptions=q textwidth=80

" Do not expand tabs.
autocmd filetype * set noexpandtab

" }

" Theme and gui settings. {

" Set colour scheme.
" You can search through your colors with ':color ' and pressing tab to
" go through them.
syntax on
set t_Co=256
set background=dark
colorscheme jellybeans

" Set up indent guides.
let g:indent_guides_color_change_percent = 5

" Show partial commands at bottom of screen.
set showcmd

" Turn on status line.
" set laststatus=2

" Highlight current line.
set cursorline

" Add 80 line marker.
set colorcolumn=80

if has('gui_running')
	" To figure out the exact font name, type :set guifont=* select your
	" font, and type ':set guifont?'.
	set guifont=Source\ Code\ Pro\ Regular\ 11

	"" Window size.
	set lines=25 columns=82

	" Automatically copy to clipboard and disable all other gui elements.
	" On command line ^R + will paste text.
	set go='a'
endif

" }

" Defines the behaviour of vim. {

" Allow unsaved buffers.
set hidden

" Allow mouse in all modes.
set mouse=a

" Hide mouse when typing.
set mousehide

" Fix backspace.
set backspace=2

" Guess how to indent next line.
set smartindent

" Avoid going to the first character on the line while moving cursor.
set nostartofline

" When word wrapping is enabled, don't jump over line don't jump over line.
nnoremap j gj
nnoremap k gk

" Highlight searches.
set hlsearch

" Highlight as you type.
set incsearch

" Automatically read changed files.
set autoread

" Make Y work from cursor to EOL.
map Y y$

" Don't make annoying noises.
"set noeb vb t_vb=
au GUIEnter * set vb t_vb=

" Set home as default directory
cd ~

" Change to current directory of file that is currently being edited.
set autochdir

" Disable word wrapping.
set nowrap

" Don't jump over wrapped lines.
noremap j gj
noremap k gk

" Don't save swap files.
set nobackup
set noswapfile
set nowb

" Make all windows even size when window resizes.
au VimResized * exe "normal \<c-w>="

" Use better menu for command line completion.
set wildmenu

" Do not show warning about saving sessions.
let g:session_autoload = 'no'
let g:session_autosave = 'no'

" Increase history limit.
set history=1000

" Increase undo limit.
set undolevels=1000

" Turn on filetype detection.
filetype on

" }

" Defines language specific settings. {

" This should serve as a good guide for other languages.

" Lisp
autocmd Filetype lisp set expandtab
autocmd Filetype lisp set tabstop=2
autocmd Filetype lisp set shiftwidth=2
autocmd Filetype lisp set softtabstop=2

" C, Perl
autocmd Filetype c set fdm=marker fmr={,} foldlevel=0
autocmd Filetype perl set fdm=marker fmr={,} foldlevel=0

" }

" vim: set fen fdm=marker fmr={,} foldlevel=0:
